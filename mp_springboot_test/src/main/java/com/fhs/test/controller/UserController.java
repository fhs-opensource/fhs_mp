package com.fhs.test.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.enums.DefaultFuncEnum;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.conditions.query.BasicJoinQueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaJoinQueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fhs.test.mapper.UserMapper;
import com.fhs.test.pojo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserMapper mapper;

    @GetMapping("/insertBatch")
    public List<User> insertBatch() {
        List<User> users = new ArrayList<>();
        User user = new User();
        // user.setUserId(30);
        user.setAge(10);
        user.setName("王磊");
        //user.setSchoolId(2);
        user.setSex("男");
        users.add(user);
        user = new User();
        user.setAge(12);
        //user.setUserId(31);
        user.setName("王磊1");
        user.setSchoolId(2);
        //user.setSex("男");
        users.add(user);
        mapper.insertBatch(users);
        return users;
    }
    @GetMapping("/updateBatch")
    public List<User> updateBatch() {
        List<User>  users = this.insertBatch();
        for (User insertBatch : users) {
            insertBatch.setSchoolId(10);
        }
        mapper.updateBatch(users);
        return users;
    }

    @GetMapping("/one")
    public User one() {
        return new User().userId().eq(1).name().like("王").age().orderByAsc().one();
    }

    @GetMapping("/querywrapper")
    public User querywrapper() {
        return mapper.selectOne(new QueryWrapper<User>().eq("school_id", 2).select("school_id").selectFun(DefaultFuncEnum.COUNT,
                "schoolCount").groupBy("school_id"));
    }

    @GetMapping("/lambdaWrapper")
    public User lambdaWrapper() {
        return mapper.selectOne(new LambdaQueryWrapper<User>().eq(User::getSchoolId, 2)
                .select(User::getSchoolId).selectFun(DefaultFuncEnum.COUNT, User::getSchoolCount).groupBy(User::getSchoolId));
    }

    @GetMapping("/queryChainWrapper")
    public User queryChainWrapper() {
        QueryChainWrapper<User> wrapper = new QueryChainWrapper<User>(mapper);
        return wrapper.eq("school_id", 2).select("school_id").selectFun(DefaultFuncEnum.COUNT,
                "schoolCount").groupBy("school_id").one();
    }

    @GetMapping("/lambdaQueryChainWrapper")
    public User lambdaQueryChainWrapper() {
        LambdaQueryChainWrapper<User> wrapper = new LambdaQueryChainWrapper<User>(mapper);
        return wrapper.eq(User::getSchoolId, 2)
                .select(User::getSchoolId).selectFun(DefaultFuncEnum.COUNT, User::getSchoolCount).groupBy(User::getSchoolId).one();
    }


    @GetMapping("/join2")
    public List<User> join2() {
        BasicJoinQueryWrapper<User> wrapper = new BasicJoinQueryWrapper<>(User.class);
        wrapper.innerJoin(School.class);
        wrapper.eq(new BasicJoinQueryWrapper.ModelProperty(User.class, "schoolId"), 2);
        wrapper.select(new BasicJoinQueryWrapper.ModelProperty(User.class, "schoolId"));
       // wrapper.selectFun(DefaultFuncEnum.COUNT, new BasicJoinQueryWrapper.ModelProperty(User.class, "schoolCount"));
    /*    wrapper.select(new BasicJoinQueryWrapper.ModelProperty(User.class,"schoolId"));
        wrapper.select(new BasicJoinQueryWrapper.ModelProperty(User.class,"userId"));
        wrapper.select(new BasicJoinQueryWrapper.ModelProperty(User.class,"name"));
        wrapper.select(new BasicJoinQueryWrapper.ModelProperty(School.class,"schoolName"));
        wrapper.select(new BasicJoinQueryWrapper.ModelProperty(School.class,"id"));
        wrapper.select(new BasicJoinQueryWrapper.ModelProperty(School.class,"remark"));*/
       // wrapper.groupBy(new BasicJoinQueryWrapper.ModelProperty(User.class, "schoolId"));
        return
                mapper.selectJoinList(wrapper);
    }

    @GetMapping("/join3")
    public List<User> join3() {
        LambdaJoinQueryWrapper<User> wrapper = new LambdaJoinQueryWrapper<>(User.class);
        wrapper.eq(User::getSchoolId, 2);
       // wrapper.select(User::getSchoolId);
        //wrapper.selectFun(DefaultFuncEnum.COUNT, User::getSchoolCount);
      // wrapper.groupBy(User::getSchoolId);
         wrapper.innerJoin(School.class).like(School::getSchoolName,"一");
        return
                mapper.selectJoinList(wrapper);
    }

    @GetMapping("/joinPager")
    public IPage<User> joinPager() {
        LambdaJoinQueryWrapper<User> wrapper = new LambdaJoinQueryWrapper<>(User.class);
        wrapper.eq(User::getSchoolId, 2);
        wrapper.select(User::getSchoolId);
        wrapper.selectFun(DefaultFuncEnum.COUNT, User::getSchoolCount);
        wrapper.groupBy(User::getSchoolId);
        /*  wrapper.innerJoin(School.class).like(School::getSchoolName,"一");*/
        Page<User> page = new Page<>(1, 10);
        return
                mapper.selectJoinPage(page,wrapper);
    }

    @GetMapping("/select")
    public List<User> select() {
        User user = new User();
        user.userId().eq(1).select(User.USERID, User.NAME, User.SEX, User.SCHOOLID).innerJoin(School.class)
                .select(School.ID, School.SCHOOLNAME, School.REMARK, School.CITYID).schoolName().like("一").innerJoin(City.class);
        return user.list();
    }


    /* @GetMapping("/one")
     public User one() {
         return User.newOBJ().userIdEQ(1).nameLike("小").one();
     }

     @GetMapping("/oneField")
     public User oneField() {
         return User.newOBJ().userIdEQ(1).nameLike("小").one(new String[]{User.USERID, User.NAME});
     }

     @GetMapping("/list")
     public List<User> list() {
         return User.newOBJ().ageBetween(10, 25).list();
     }

     @GetMapping("/listField")
     public List<User> listField() {
         return User.newOBJ().ageBetween(10, 20).list(new String[]{User.USERID, User.NAME});
     }

     @GetMapping("/delete")
     public int delete() {
         return User.newOBJ().ageBetween(50, 80).delete();
     }

     @GetMapping("/count")
     public Long count() {
         return  User.newOBJ().ageBetween(10,26).count();
     }


     @GetMapping("/update")
     public int update() {
         User user = User.newOBJ();
         user.setAge(19);
         return user.nameEQ("小明").update();
     }*/
  /*  @Autowired
    private User.UserMapper mapper;

    @GetMapping("/test")
    public Long test() {
        return mapper.selectCount(new LambdaQueryWrapper<User>());
    }*/
}
