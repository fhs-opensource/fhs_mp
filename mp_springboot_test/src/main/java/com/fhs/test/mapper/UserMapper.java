package com.fhs.test.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fhs.test.pojo.User;
import org.springframework.stereotype.Repository;

/**
 * 测试mybatis plus翻译
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

}

