package com.fhs.test.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value="city",genMapper = true,autoResultMap = true)
public class City {

    @TableId(value="id")
    private Integer id;

    @TableField(value="city_name")
    private String cityName;
}
