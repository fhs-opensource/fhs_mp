package com.fhs.test.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

@Data
@TableName(value="school",genMapper = true)
public class School {
    @TableId
    private Integer id;

    @TableField("school_name")
    private String schoolName;

    @TableField("remark")
    private String remark;

    @TableField("city_id")
    private Integer cityId;

    @TableField(target = City.class,relation = Relation.MANY_TO_ONE,modelFields = "cityId")
    private City city;

    @TableLogic
    @TableField(value="is_delete",fill = FieldFill.INSERT)
    private Integer isDelete;
}
