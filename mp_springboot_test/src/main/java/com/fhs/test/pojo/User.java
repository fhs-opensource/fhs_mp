package com.fhs.test.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.List;

@Data
//@KeySequence(value = "user_serial")
@TableName(value="user",genMapper = true)
public class User {

    @TableId(value="user_id",type = IdType.AUTO)
    private Integer userId;

    @TableField("name")
    private String name;

    @TableField(exist = false,funField = true)
    private Long schoolCount;

    @TableField("age")
    private Integer age;

    @TableField("sex")
    private String sex;

    @TableField(relation = Relation.ONE_TO_MANY,target = School.class,modelFields = "schoolId")
    List<School> schools;

    @TableField(value="school_id")
    private Integer schoolId;

    @TableLogic
    @TableField(value="is_delete",fill = FieldFill.INSERT)
    private Integer isDelete;

}
