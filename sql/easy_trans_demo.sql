/*
 Navicat Premium Data Transfer

 Source Server         : 10.102.2.201
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : 10.102.2.201:3306
 Source Schema         : easy_trans_demo

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 24/03/2022 14:43:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for city
-- ----------------------------
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city`  (
  `id` int(11) NOT NULL,
  `city_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of city
-- ----------------------------
INSERT INTO `city` VALUES (3, '西安');

-- ----------------------------
-- Table structure for school
-- ----------------------------
DROP TABLE IF EXISTS `school`;
CREATE TABLE `school`  (
  `id` int(32) NOT NULL,
  `school_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `city_id` int(32) NULL DEFAULT NULL,
  `is_delete` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of school
-- ----------------------------
INSERT INTO `school` VALUES (1, '第一中学', '2222', 3, 0);
INSERT INTO `school` VALUES (2, '高新一小', '2222', 3, 0);
INSERT INTO `school` VALUES (3, '铁一中', '2222', 3, 0);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` int(32) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `age` int(10) NULL DEFAULT NULL,
  `sex` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `school_id` int(2) NULL DEFAULT NULL,
  `is_delete` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 39 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '王磊', 30, '男', 2, 0);
INSERT INTO `user` VALUES (2, '丽丽', 25, '女', 2, 0);
INSERT INTO `user` VALUES (3, '王磊', 10, '男', 2, 0);
INSERT INTO `user` VALUES (4, '王磊1', 12, '男', 2, 0);
INSERT INTO `user` VALUES (5, '王磊', 10, '男', 2, 0);
INSERT INTO `user` VALUES (6, '王磊1', 12, '男', 2, 0);
INSERT INTO `user` VALUES (7, '王磊', 10, '男', 2, 0);
INSERT INTO `user` VALUES (8, '王磊1', 12, '男', 2, 0);
INSERT INTO `user` VALUES (9, '王磊', 10, '男', 2, 0);
INSERT INTO `user` VALUES (10, '王磊', 10, '男', 2, 0);
INSERT INTO `user` VALUES (11, '王磊', 10, '男', 2, 0);
INSERT INTO `user` VALUES (12, '王磊1', 12, '男', 2, 0);
INSERT INTO `user` VALUES (13, '王磊', 10, '男', 2, 0);
INSERT INTO `user` VALUES (14, '王磊1', 12, '男', 2, 0);
INSERT INTO `user` VALUES (15, '王磊', 10, '男', 2, 0);
INSERT INTO `user` VALUES (16, '王磊1', 12, '男', 2, 0);
INSERT INTO `user` VALUES (17, '王磊', 10, '男', 2, 0);
INSERT INTO `user` VALUES (18, '王磊1', 12, '男', 2, 0);
INSERT INTO `user` VALUES (19, '王磊', 10, '男', 2, 0);
INSERT INTO `user` VALUES (20, '王磊1', 12, '男', 2, 0);
INSERT INTO `user` VALUES (21, '王磊', 10, '男', 2, 0);
INSERT INTO `user` VALUES (22, '王磊1', 12, '男', 2, 0);
INSERT INTO `user` VALUES (23, '王磊', 10, '男', 2, 0);
INSERT INTO `user` VALUES (24, '王磊1', 12, '男', 2, 0);
INSERT INTO `user` VALUES (25, '王磊', 10, '男', 10, 0);
INSERT INTO `user` VALUES (26, '王磊1', 12, '男', 10, 0);
INSERT INTO `user` VALUES (27, '王磊', 10, '男', NULL, 0);
INSERT INTO `user` VALUES (28, '王磊1', 12, NULL, 2, 0);
INSERT INTO `user` VALUES (29, '王磊', 10, '男', 10, 0);
INSERT INTO `user` VALUES (30, '王磊1', 12, NULL, 10, 0);
INSERT INTO `user` VALUES (31, '王磊', 10, '男', 10, 0);
INSERT INTO `user` VALUES (32, '王磊1', 12, NULL, 10, 0);
INSERT INTO `user` VALUES (33, '王磊', 10, '男', 10, 0);
INSERT INTO `user` VALUES (34, '王磊1', 12, NULL, 10, 0);
INSERT INTO `user` VALUES (35, '王磊', 10, '男', 10, NULL);
INSERT INTO `user` VALUES (36, '王磊1', 12, NULL, 10, NULL);
INSERT INTO `user` VALUES (37, '王磊', 10, '男', NULL, 0);
INSERT INTO `user` VALUES (38, '王磊1', 12, NULL, 2, 0);

SET FOREIGN_KEY_CHECKS = 1;
