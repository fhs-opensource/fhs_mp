package com.test;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;


/**
 * 报错先看readme.md
 *
 * @author wanglei
 */
@Data
@TableName(value = "student", genMapper = true)
public class Student {

    @TableId(value = "id")
    private Long id;
    private Long userId;
    private String name;
    private Integer age;


}
