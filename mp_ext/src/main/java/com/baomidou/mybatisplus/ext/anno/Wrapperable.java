package com.baomidou.mybatisplus.ext.anno;

import java.lang.annotation.*;

/**
 * 标记pojo 自动生成 wrapper操作相关方法
 * 这会让你的po很重，请做好心理准备
 * @author wanglei
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Wrapperable {
}
